dset $dataset
dtype hdf5_grid
title IMERG sample
undef -9999.9
xdef 3600 linear -179.95 0.1
ydef 1800 linear -89.95 0.1
zdef 1 levels 1 1
tdef 1 linear 00:00Z09jun2015 30mn
vars 5
/Grid/HQprecipitation=>HQp 0 x,y HQ precipitation
/Grid/IRprecipitation=>IRp 0 x,y IR precipitation
/Grid/precipitationCal=>Calp 0 x,y cal precipitation
/Grid/precipitationUncal=>Uncalp 0 x,y uncal precipitation
/Grid/randomError=>error 0 x,y random error
Endvars