*ncols 622
*nrows 911
*nbands 1
*nbits 8
*cellsize 5007
*xllcenter -1554224.7
*yllcenter -3118347.1
*nodata_value 0
*pixeltype SIGNEDBYTE
dset /Volumes/MacOSXJ/DATA/AMZEX/3B42RT/2012/TRMM_bin/01/3B42RT.2012010100.7R2.bin
*dset ^3B42RT.%y4%m2%d2%h2.7.bin

options template big_endian yrev

title Real-Time Three Hourly TRMM and Other Satellite Rainfall (3B42RT)

headerbytes 2880

undef -31999

xdef 1440 linear 0.125 0.25

ydef 480  linear -59.875 0.25

zdef 1 levels 1

tdef 99999 linear 21Z23sep2006 3hr

vars 3

p       0 -1,40,2,-1 Precipitation (mm/hr)

perror  0 -1,40,2,-1 Precipitation Error (mm/hr)

source  0 -1,40,1    Source Number

endvars