#! /bin/zsh

#####ncftp must be installed prior to executing the following script#####
#get_MOD07.sh reads filenames from textfile "/FIRERISK/DATA/AMZ/MOD07L2/YYYY/MOD07_L2.AYYYYJJJ.txt" 
#and pulls files from ftp site ladsweb.nascom.nasa.gov
#input args are 1-year (YYYY) 2-julian day (JJJ) 
year=$1
jday=$2
echo "Getting MOD07"
cd /firecast/DATA/AMZEX/MOD07L2/${year}/
while read file 
do
echo $file 
ncftp ladsweb.nascom.nasa.gov << FTP
cd /allData/6/MOD07_L2/${year}/${jday}/
mget ${file}
quit
FTP
done < MOD07_L2.A${year}${jday}amzex.txt
